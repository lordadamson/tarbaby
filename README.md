# tarbaby
a script that lets you untar any tar archive without having to memorize the command options

## Installation
make sure the script has an execute permession: </br>
`chmod +x tarbaby.sh` </br>

To install the script (tarbaby.sh) just copy the script any where in your $PATH. For example: `/usr/bin/` </br>

## Sample Usage
well that's pretty simple, just type `tarbaby` and then the name of the archive you'd like to decompress.
### Example:
`tarbaby file-name.tar.gz`

## Supported Formats:
* tar.gz
* tar.gz2
* tar.bz2
* tar.xz

## TODO
Add more formats.
